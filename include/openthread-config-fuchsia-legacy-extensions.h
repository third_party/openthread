/*
 *  Copyright (c) 2018, The OpenThread Authors.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the copyright holder nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef OPENTHREAD_INCLUDE_OPENTHREAD_CONFIG_FUCHSIA_LEGACY_EXTENSIONS_H_
#define OPENTHREAD_INCLUDE_OPENTHREAD_CONFIG_FUCHSIA_LEGACY_EXTENSIONS_H_

#include "openthread-config-fuchsia.h"

/**
 * @def OPENTHREAD_CONFIG_LEGACY_ENABLE
 * Define to 1 if you want to use legacy network support
 *
 */
#define OPENTHREAD_CONFIG_LEGACY_ENABLE 1

/**
 * @def OPENTHREAD_ENABLE_VENDOR_EXTENSION
 * Define to 1 if you want to use any kind of extensions like legacy network support.
 *
 */
#define OPENTHREAD_ENABLE_VENDOR_EXTENSION 1

/*
 * Implementation note:
 *   These are all "weak" so that a platform may if it chooses to override the instance.
 */

#if OPENTHREAD_CONFIG_OTNS_ENABLE

#include <openthread/platform/toolchain.h>
#include "common/log.hpp"

using namespace ot;

OT_TOOL_WEAK
void otPlatOtnsStatus(const char *aStatus)
{
    LogAlways("[OTNS] %s", aStatus);
}
#endif // OPENTHREAD_CONFIG_OTNS_ENABLE

#endif  // OPENTHREAD_INCLUDE_OPENTHREAD_CONFIG_FUCHSIA_LEGACY_EXTENSIONS_H_
